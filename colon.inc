%define next_elem 0
%macro colon 2
    %ifid %1
        %ifstr %2
            %1:
                dq next_elem
                db %2, 0
            %define next_elem %1
        %endif
    %endif
%endmacro
