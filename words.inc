%include "colon.inc"

section .rodata

colon phone, `Phone`
db "Phone word", 0

colon paper, 'Paper'
db "Paper word", 0

colon porshe, `Porshe`
db "Porshe word", 0

colon lamborgini, `Lamborgini two`
db "Lamborgini word", 0
