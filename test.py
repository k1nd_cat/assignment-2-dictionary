import subprocess

PROGRAM_NAME = "./main"

list_input = [
    "", 
    "Porshe", 
    "Lamborgini two", 
    "Hell", 
    "Семь раз отмерь, один отрежь... | Дождливая погода. Из дома выходить совершенно не хочется... | Удивительные события происходят на улице... | Порой хочется забыть предыдущий день, но его надо лишь воспринимать, как опыт.",
    "Б" * 255, "В" * 300
]

list_output = ["", "Porshe word", "Lamborgini word", "", "", "", ""]
list_error = ["There is no such key", "", "", "There is no such key", "There is no such key", "There is no such key", "There is no such key"]
for i in range (len(list_input)):
    # создание подпроцесса
    sp = subprocess.Popen([PROGRAM_NAME], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = sp.communicate(input=list_input[i].encode())
    out = stdout.decode().strip()
    err = stderr.decode().strip()
    ret_c = sp.returncode
    if (out == list_output[i] and err == list_error[i]):
        print("Test ", i + 1,": Success")
        if (out == ""):
            print("Это вывелось в error: ", err)
        else:
            print("Это вывелось в out: ", out)
    else:
        print(out)
        print(err)
        print("Test: ", i + 1, ": Fail")
