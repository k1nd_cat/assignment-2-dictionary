%include "dict.inc"
%include "lib.inc"
%include "words.inc"

section .rodata
key_not_found:    db "There is no such key", 0x0
%define SIZE 256
%define BUFFER_SIZE 255

section .bss
buffer_main: resb BUFFER_SIZE

section .text

global _start
_start:
        mov rdi, buffer_main
        mov rsi, SIZE
        call read_word
        mov rdi, buffer_main
        mov rsi, lamborgini
        call find_word
        test rax, rax
        je .not_found_main
        mov rdi, rax
        add rdi, 8
        call string_length
        add rdi, rax
        inc rdi
        call print_string
        call print_newline
        jmp exit
.not_found_main:
        mov rdi, key_not_found
        call print_str_err
        jmp exit
