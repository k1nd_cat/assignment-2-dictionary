section .bss

%define bufsize 1024
%define SYSCALL_OUT 1
%define STDOUT_DS 1
%define STDIN_DS 0
%define STDERROR_DS 2
buffer: resb bufsize

section .text


global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_str_err

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
       je .string_len_ret
        inc rax
        jmp .loop
    .string_len_ret:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        push rdi
        call string_length
        mov rdx, rax
        mov rax, SYSCALL_OUT
        pop rsi
        mov rdi, STDOUT_DS
        syscall
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
        mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
        push rdi
        mov rax, SYSCALL_OUT
        mov rdi, STDOUT_DS
        mov rdx, 1
        mov rsi, rsp
        syscall
        pop rdi
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
        test rdi, rdi
        jns print_uint
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        not rdi
        inc rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; Данное число вводится через rdi
print_uint:
        mov rcx, 1
        mov r10, 0xA
        mov rax, rdi
        dec rsp
        mov byte [rsp], 0
        .loop:
                mov rdx, 0
                div r10
                add dl, '0'
                inc rcx
                dec rsp
                mov [rsp], dl
                cmp rax, 0
                jne .loop
        .out:
                mov rdi, rsp
                push rcx
                call print_string
                pop rcx
                add rsp, rcx
                ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; Данные на входе хранятся в rdi и rsi
string_equals:
    xor         rax, rax
    .loop:
        mov r8b, [rdi]
        mov r9b, [rsi]
        cmp r8b, r9b
        jne .zero
        test r8b, r8b
        jz .str_end
        inc rdi
        inc rsi
        jmp .loop
    .zero:
        ret
    .str_end:
        inc rax
        ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        xor rax, rax
        mov rdi, STDIN_DS
        mov rsi, buffer
        mov rdx, 1
        syscall
        test rax, rax
        jz .read_char_ret
        mov al, byte[buffer]
        ret
        .read_char_ret:
                xor rax, rax
                ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
        push r12
        push r13
        push r14
        mov r12, rdi
        mov r13, rsi
        xor r14, r14
        .read_first_char:
                call read_char
                cmp al, `\t`
                je .read_first_char
                cmp al, `\n`
                je .read_first_char
                cmp al, ' '
                je .read_first_char
        .loop:
                cmp r14, r13
                je .read_char_exit
                test al, al
                jz .loop_exit
                cmp al, `\t`
                je .loop_exit
                cmp al, `\n`
                je .loop_exit
                mov [r12+r14], al
                inc r14
                call read_char
                jmp .loop
        .read_char_exit:
                xor rax, rax
                jmp .popaem
        .loop_exit:
                mov rax, r12
                mov byte [r12+r14], 0
                mov rdx, r14
        .popaem:
                pop r14
                pop r13
                pop r12
                ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rcx, rcx
        mov r10, 0xA
        xor r9, r9
        xor rax, rax
        .loop_parse_uint:
                mov cl, [rdi + r9]
                sub cl, '0'
                test cl, cl
                jl .lower_or_higher
                cmp cl, 9
                jg .lower_or_higher
                inc r9
                mul r10
                add rax, rcx
                jmp .loop_parse_uint
.lower_or_higher:
        mov rdx, r9
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
        mov r10, rdi
        mov al, [r10]
        cmp al, '-'
        je .negative_number
        cmp al, '+'
        je .with_plus
        mov r10, rdi
        call parse_uint
        ret
        .negative_number:
                inc r10
                mov rdi, r10
                call parse_uint
                neg rax
                inc rdx
                ret
        .with_plus:
                inc r10
                mov rdi, r10
                call parse_uint
                ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        mov rcx, 0
        .loop_string_copy:
                mov al, [rdi + rcx]
                mov [rsi + rcx], al
                inc rcx
                test al, al
                jz .ret_str_copy
                jmp .loop_string_copy
        .ret_str_copy:
                cmp rcx, rdx
                jg .end_str_copy
                mov rax, rcx
                ret
        .end_str_copy:
                xor rax, rax
                ret
print_str_err:
        push rdi
        call string_length
        mov rdx, rax
        mov rax, SYSCALL_OUT
        pop rsi
        mov rdi, STDERROR_DS
        syscall
        ret
