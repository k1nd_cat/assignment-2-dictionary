%include "lib.inc"

%define SIZE 8

section .text

global find_word

find_word:
        push r11
        push r12
        mov r11, rdi
        mov r12, rsi
        .loop:
                mov rdi, r11
                mov rsi, r12
                add rsi, SIZE
                call string_equals
                test rax, rax
                jne .end_find_word
                cmp qword[r12], 0
                mov r12, qword[r12]
                jne .loop
        .end_find_word:
                mov rax, r12
                pop r12
                pop r11
                ret
